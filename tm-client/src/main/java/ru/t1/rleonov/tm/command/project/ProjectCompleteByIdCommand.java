package ru.t1.rleonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ProjectCompleteByIdRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String COMMAND = "project-complete-by-id";

    @NotNull
    private static final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().completeProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
