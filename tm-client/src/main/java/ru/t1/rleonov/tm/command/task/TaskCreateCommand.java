package ru.t1.rleonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.TaskCreateRequest;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private static final String COMMAND = "task-create";

    @NotNull
    private static final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().createTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
