package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.repository.dto.TaskDTORepository;
import ru.t1.rleonov.tm.service.ConnectionService;
import ru.t1.rleonov.tm.service.PropertyService;
import ru.t1.rleonov.tm.service.dto.ProjectDTOService;
import ru.t1.rleonov.tm.service.dto.TaskDTOService;
import ru.t1.rleonov.tm.service.dto.UserDTOService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.ADMIN;

@Category(UnitCategory.class)
public final class TaskDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROPERTY_SERVICE,
            PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @NotNull
    private static ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String ADMIN_ID = "";

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER2_ID = USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail()).getId();
        ADMIN_ID = USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail()).getId();
        ADMIN_PROJECT1.setUserId(ADMIN_ID);
        PROJECT_SERVICE.add(ADMIN_PROJECT1);
    }

    @AfterClass
    @SneakyThrows
    public static void reset() {
        PROJECT_SERVICE.clear();
        USER_SERVICE.clear();
        CONNECTION_SERVICE.getEntityManagerFactory().close();
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        USER1_TASKS.forEach(task -> task.setUserId(USER1_ID));
        USER2_TASKS.forEach(task -> task.setUserId(USER2_ID));
        ADMIN_TASKS.forEach(task -> task.setUserId(ADMIN_ID));
        try {
            transaction.begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER2_TASK1);
            repository.add(USER2_TASK2);
            repository.add(ADMIN_TASK1);
            repository.add(ADMIN_TASK2);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void after() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        USER1_TASK3.setUserId(USER1_ID);
        try {
            transaction.begin();
            repository.add(USER1_TASK3);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable final String user_id = USER1_TASK3.getUserId();
        @Nullable final String id = USER1_TASK3.getId();
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(user_id, USER1_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(user_id, task.getUserId());
        Assert.assertEquals(id, task.getId());
    }

    @Test
    @SneakyThrows
    public void update() {
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @Nullable final String id = USER1_TASK1.getId();
        USER1_TASK1.setName("TEST");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(USER1_TASK1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(user_id, id);
        Assert.assertNotNull(task);
        Assert.assertEquals("TEST", task.getName());
    }

    @Test
    @SneakyThrows
    public void set() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        TASK_SERVICE.clear();
        try {
            transaction.begin();
            repository.set(USER1_TASKS);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull final List<TaskDTO> tasks = TASK_SERVICE.findAll();
        Assert.assertEquals(USER1_TASKS.size(), tasks.size());
        Assert.assertEquals(USER1_TASKS.get(0).getId(), tasks.get(0).getId());
        Assert.assertEquals(USER1_TASKS.get(1).getId(), tasks.get(1).getId());
    }

    @Test
    @SneakyThrows
    public void remove() {
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @Nullable final String id = USER1_TASK1.getId();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(USER1_TASK1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(user_id, id);
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull List<TaskDTO> tasks;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            tasks = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(6, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @Nullable final String user_id = USER1_TASK1.getUserId();
        Assert.assertNotNull(user_id);
        @Nullable final String id = USER1_TASK1.getId();
        @Nullable TaskDTO task;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            task = repository.findOneById(user_id, id);
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(task);
        Assert.assertEquals(user_id, task.getUserId());
        Assert.assertEquals(id, task.getId());
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull List<TaskDTO> tasks;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            tasks = repository.findAllByProjectId(ADMIN_ID, ADMIN_PROJECT1.getId());
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(0, TASK_SERVICE.findAll().size());
    }
    
}
