package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    public void getDbSecondLvlCash() {
        Assert.assertNotNull(propertyService.getDbSecondLvlCash());
    }

    @Test
    public void getDbSchema() {
        Assert.assertNotNull(propertyService.getDbSchema());
    }

    @Test
    public void getDbFactoryClass() {
        Assert.assertNotNull(propertyService.getDbFactoryClass());
    }

    @Test
    public void getDbUseQueryCash() {
        Assert.assertNotNull(propertyService.getDbUseQueryCash());
    }

    @Test
    public void getDbUseMinPuts() {
        Assert.assertNotNull(propertyService.getDbUseMinPuts());
    }

    @Test
    public void getDbRegionPrefix() {
        Assert.assertNotNull(propertyService.getDbRegionPrefix());
    }

    @Test
    public void getDbHazelConfig() {
        Assert.assertNotNull(propertyService.getDbHazelConfig());
    }

    @Test
    public void getDbDialect() {
        Assert.assertNotNull(propertyService.getDbDialect());
    }

    @Test
    public void getDbDdlAuto() {
        Assert.assertNotNull(propertyService.getDbDdlAuto());
    }

    @Test
    public void getDbShowSql() {
        Assert.assertNotNull(propertyService.getDbShowSql());
    }

    @Test
    public void getDbFormatSql() {
        Assert.assertNotNull(propertyService.getDbFormatSql());
    }

    @Test
    public void getDbDriver() {
        Assert.assertNotNull(propertyService.getDbDriver());
    }

    @Test
    public void getDbLogin() {
        Assert.assertNotNull(propertyService.getDbLogin());
    }

    @Test
    public void getDbPassword() {
        Assert.assertNotNull(propertyService.getDbPassword());
    }

    @Test
    public void getDbUrl() {
        Assert.assertNotNull(propertyService.getDbUrl());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

}
