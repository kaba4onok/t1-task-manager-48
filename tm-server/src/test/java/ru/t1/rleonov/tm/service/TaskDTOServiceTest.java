package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.service.dto.ProjectDTOService;
import ru.t1.rleonov.tm.service.dto.TaskDTOService;
import ru.t1.rleonov.tm.service.dto.UserDTOService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.ADMIN;

@Category(UnitCategory.class)
public final class TaskDTOServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROPERTY_SERVICE,
            PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String ADMIN_ID = "";

    @NotNull
    private final static List<ProjectDTO> PROJECTS = new ArrayList<>();

    @NotNull
    private final static List<TaskDTO> TASKS = new ArrayList<>();

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER2_ID = USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail()).getId();
        ADMIN_ID = USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail()).getId();
        USER1_PROJECTS.forEach(project -> project.setUserId(USER1_ID));
        USER2_PROJECTS.forEach(project -> project.setUserId(USER2_ID));
        ADMIN_PROJECTS.forEach(project -> project.setUserId(ADMIN_ID));
        PROJECTS.addAll(USER1_PROJECTS);
        PROJECTS.addAll(USER2_PROJECTS);
        PROJECTS.addAll(ADMIN_PROJECTS);
        PROJECT_SERVICE.set(PROJECTS);
    }

    @AfterClass
    @SneakyThrows
    public static void reset() {
        PROJECT_SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Before
    @SneakyThrows
    public void before() {
        USER1_TASKS.forEach(task -> task.setUserId(USER1_ID));
        USER2_TASKS.forEach(task -> task.setUserId(USER2_ID));
        ADMIN_TASKS.forEach(task -> task.setUserId(ADMIN_ID));
        USER1_TASK3.setUserId(USER1_ID);
        TASKS.addAll(USER1_TASKS);
        TASKS.addAll(USER2_TASKS);
        TASKS.addAll(ADMIN_TASKS);
        TASK_SERVICE.set(TASKS);
    }

    @After
    @SneakyThrows
    public void after() {
        TASK_SERVICE.clear();
    }

    @Test
    public void create() {
        @Nullable final String userId = USER1_TASK3.getUserId();
        @NotNull final String name = USER1_TASK3.getName();
        @NotNull final String desc = USER1_TASK3.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create("", name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, "", desc));
        Assert.assertEquals(name, TASK_SERVICE.create(userId, name, desc).getName());
    }

    @Test
    public void updateById() {
        @Nullable final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        @NotNull final String name = USER1_TASK1.getName();
        @NotNull final String desc = USER1_TASK1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, id, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById("", id, name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(userId, null, name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(userId, "", name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(userId, id, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(userId, id, "", desc));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                TASK_SERVICE.updateById(WRONG_USERID_TASK.getUserId(), id, name, desc));
        Assert.assertEquals(USER1_TASK2.getName(),
                TASK_SERVICE.updateById(userId, id, USER1_TASK2.getName(), desc).getName());
    }

    @Test
    public void removeById() {
        @Nullable final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(userId, ""));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.removeById("0", id));
        Assert.assertEquals(id, TASK_SERVICE.removeById(userId, id).getId());
    }

    @Test
    public void changeTaskStatusById() {
        @Nullable final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, id, status));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById("", id, status));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(userId, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(userId, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(userId, id, null));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                TASK_SERVICE.changeTaskStatusById(WRONG_USERID_TASK.getUserId(), id, status));
        Assert.assertEquals(status, TASK_SERVICE.changeTaskStatusById(userId, id, status).getStatus());
    }

    @Test
    public void clear() {
        TASK_SERVICE.clear();
        Assert.assertEquals(Collections.emptyList(), TASK_SERVICE.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(6, TASK_SERVICE.findAll().size());
    }

    @Test
    public void findAllByProjectId() {
        @Nullable final String userId = ADMIN_PROJECT1.getUserId();
        @Nullable final String projectId = ADMIN_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(null, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId("", projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(userId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(userId, ""));
        Assert.assertEquals(2, TASK_SERVICE.findAllByProjectId(userId, projectId).size());
    }

    @Test
    public void findOneById() {
        @Nullable final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findOneById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findOneById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findOneById(userId, ""));
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(userId, id);
        Assert.assertNotNull(task);
        Assert.assertEquals(id, task.getId());
    }

    @Test
    public void set() {
        TASK_SERVICE.clear();
        TASK_SERVICE.set(USER1_TASKS);
        Assert.assertEquals(USER1_TASKS.size(), TASK_SERVICE.findAll().size());
    }

}
