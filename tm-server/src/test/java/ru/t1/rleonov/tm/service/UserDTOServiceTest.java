package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import ru.t1.rleonov.tm.exception.entity.UserNotFoundException;
import ru.t1.rleonov.tm.exception.field.EmailEmptyException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.LoginEmptyException;
import ru.t1.rleonov.tm.exception.field.PasswordEmptyException;
import ru.t1.rleonov.tm.exception.user.LoginExistsException;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.service.dto.ProjectDTOService;
import ru.t1.rleonov.tm.service.dto.TaskDTOService;
import ru.t1.rleonov.tm.service.dto.UserDTOService;
import ru.t1.rleonov.tm.util.HashUtil;
import java.util.Collections;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserDTOServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROPERTY_SERVICE,
            PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER1_ID = "";

    @Before
    @SneakyThrows
    public void before() {
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail());
        USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail());
    }

    @After
    @SneakyThrows
    public void after() {
        USER_SERVICE.clear();
    }

    private void initProjectsTasks() {
        USER1_PROJECTS.forEach(project -> project.setUserId(USER1_ID));
        PROJECT_SERVICE.set(USER1_PROJECTS);
        USER1_TASKS.forEach(task -> task.setUserId(USER1_ID));
        TASK_SERVICE.set(USER1_TASKS);
    }

    @Test
    public void create() {
        @Nullable final String login = USER1.getLogin();
        @Nullable final String password = USER1.getLogin();
        @Nullable final String email = USER1.getEmail();
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, password, email));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create("", password, email));
        Assert.assertThrows(LoginExistsException.class, () -> USER_SERVICE.create(login, password, email));
        USER_SERVICE.clear();
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(login, null, email));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(login, "", email));
        Assert.assertEquals(USER1.getLogin(), USER_SERVICE.create(login, password, email).getLogin());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(3, USER_SERVICE.findAll().size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.findOneById(""));
        @Nullable final UserDTO user = USER_SERVICE.findOneById(USER1_ID);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1_ID, user.getId());
    }

    @Test
    public void findByLogin() {
        @Nullable final String login = USER1.getLogin();
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(""));
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(login);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1_ID, user.getId());
    }

    @Test
    public void findByEmail() {
        @Nullable final String email = USER1.getEmail();
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.findByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.findByEmail(""));
        @Nullable final UserDTO user = USER_SERVICE.findByEmail(email);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1_ID, user.getId());
    }

    @Test
    public void removeByLogin() {
        @Nullable final String login = USER1.getLogin();
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.removeByLogin(USERTEST.getLogin()));
        initProjectsTasks();
        Assert.assertNotNull(TASK_SERVICE.findAll().get(0));
        Assert.assertNotNull(PROJECT_SERVICE.findAll().get(0));
        USER_SERVICE.removeByLogin(login);
        Assert.assertEquals(Collections.EMPTY_LIST, TASK_SERVICE.findAll());
        Assert.assertEquals(Collections.EMPTY_LIST, PROJECT_SERVICE.findAll());
        Assert.assertNull(USER_SERVICE.findByLogin(login));
    }

    @Test
    public void removeByEmail() {
        @Nullable final String email = USER1.getEmail();
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(""));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.removeByEmail(USERTEST.getEmail()));
        initProjectsTasks();
        Assert.assertNotNull(TASK_SERVICE.findAll().get(0));
        Assert.assertNotNull(PROJECT_SERVICE.findAll().get(0));
        USER_SERVICE.removeByEmail(email);
        Assert.assertEquals(Collections.EMPTY_LIST, TASK_SERVICE.findAll());
        Assert.assertEquals(Collections.EMPTY_LIST, PROJECT_SERVICE.findAll());
        Assert.assertNull(USER_SERVICE.findByEmail(email));
    }

    @Test
    public void setPassword() {
        @NotNull final String id = USER1_ID;
        @Nullable final String password = USERTEST.getPassword();
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, password));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword("", password));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(id, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(id, ""));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(USERTEST.getId(), password));
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, password), USER_SERVICE.setPassword(id, password).getPassword());
    }

    @Test
    public void updateUser() {
        @NotNull final String id = USER1_ID;
        @Nullable final String fname = USER2.getFirstName();
        @Nullable final String lname = USER2.getLastName();
        @Nullable final String mname = USER2.getMiddleName();
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser(null, fname, lname, mname));
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser("", fname, lname, mname));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.updateUser(USERTEST.getId(), fname, lname, mname));
        USER_SERVICE.updateUser(id, fname, lname, mname);
        @Nullable final UserDTO user = USER_SERVICE.findOneById(id);
        Assert.assertNotNull(user);
        Assert.assertEquals(fname, user.getFirstName());
        Assert.assertEquals(lname, user.getLastName());
        Assert.assertEquals(mname, user.getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(USER_SERVICE.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(USER_SERVICE.isLoginExist(null));
        Assert.assertFalse(USER_SERVICE.isLoginExist(""));
        Assert.assertFalse(USER_SERVICE.isLoginExist(USERTEST.getLogin()));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(USER_SERVICE.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(USER_SERVICE.isEmailExist(null));
        Assert.assertFalse(USER_SERVICE.isEmailExist(""));
        Assert.assertFalse(USER_SERVICE.isEmailExist(USERTEST.getEmail()));
    }

    @Test
    public void lockUserByLogin() {
        @Nullable final String login = USER1.getLogin();
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(login);
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.lockUserByLogin(USERTEST.getLogin()));
        Assert.assertTrue(USER_SERVICE.lockUserByLogin(login).getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @Nullable final String login = USER1.getLogin();
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(login);
        Assert.assertNotNull(user);
        user.setLocked(true);
        Assert.assertTrue(user.getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.unlockUserByLogin(USERTEST.getLogin()));
        Assert.assertFalse(USER_SERVICE.unlockUserByLogin(login).getLocked());
    }

    @Test
    public void set() {
        USER_SERVICE.clear();
        USER_SERVICE.set(USERS);
        Assert.assertEquals(USERS.size(), USER_SERVICE.findAll().size());
    }

    @Test
    public void clear() {
        USER_SERVICE.clear();
        Assert.assertEquals(Collections.emptyList(), USER_SERVICE.findAll());
    }

}
