package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.repository.dto.ProjectDTORepository;
import ru.t1.rleonov.tm.service.ConnectionService;
import ru.t1.rleonov.tm.service.PropertyService;
import ru.t1.rleonov.tm.service.dto.ProjectDTOService;
import ru.t1.rleonov.tm.service.dto.TaskDTOService;
import ru.t1.rleonov.tm.service.dto.UserDTOService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROPERTY_SERVICE,
            PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @NotNull
    private static IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String ADMIN_ID = "";

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER2_ID = USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail()).getId();
        ADMIN_ID = USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail()).getId();
    }

    @AfterClass
    @SneakyThrows
    public static void reset() {
        PROJECT_SERVICE.clear();
        USER_SERVICE.clear();
        CONNECTION_SERVICE.getEntityManagerFactory().close();
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        USER1_PROJECTS.forEach(project -> project.setUserId(USER1_ID));
        USER2_PROJECTS.forEach(project -> project.setUserId(USER2_ID));
        ADMIN_PROJECTS.forEach(project -> project.setUserId(ADMIN_ID));
        try {
            transaction.begin();
            repository.add(USER1_PROJECT1);
            repository.add(USER1_PROJECT2);
            repository.add(USER2_PROJECT1);
            repository.add(USER2_PROJECT2);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void after() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(USER1_PROJECT3);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable final String user_id = USER1_PROJECT3.getUserId();
        @Nullable final String id = USER1_PROJECT3.getId();
        @Nullable final ProjectDTO project = PROJECT_SERVICE.findOneById(user_id, USER1_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(user_id, project.getUserId());
        Assert.assertEquals(id, project.getId());
    }

    @Test
    @SneakyThrows
    public void update() {
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @Nullable final String id = USER1_PROJECT1.getId();
        USER1_PROJECT1.setName("TEST");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(USER1_PROJECT1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable final ProjectDTO project = PROJECT_SERVICE.findOneById(user_id, id);
        Assert.assertNotNull(project);
        Assert.assertEquals("TEST", project.getName());
    }

    @Test
    @SneakyThrows
    public void set() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        PROJECT_SERVICE.clear();
        try {
            transaction.begin();
            repository.set(ADMIN_PROJECTS);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull final List<ProjectDTO> projects = PROJECT_SERVICE.findAll();
        Assert.assertEquals(ADMIN_PROJECTS.size(), projects.size());
        Assert.assertEquals(ADMIN_PROJECTS.get(0).getId(), projects.get(0).getId());
        Assert.assertEquals(ADMIN_PROJECTS.get(1).getId(), projects.get(1).getId());
    }

    @Test
    @SneakyThrows
    public void remove() {
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @Nullable final String id = USER1_PROJECT1.getId();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(USER1_PROJECT1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable final ProjectDTO project = PROJECT_SERVICE.findOneById(user_id, id);
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull List<ProjectDTO> projects;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        try {
            projects = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(4, projects.size());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        Assert.assertNotNull(user_id);
        @Nullable final String id = USER1_PROJECT1.getId();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        try {
            project = repository.findOneById(user_id, id);
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(project);
        Assert.assertEquals(user_id, project.getUserId());
        Assert.assertEquals(id, project.getId());
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(0, PROJECT_SERVICE.findAll().size());
    }

}
