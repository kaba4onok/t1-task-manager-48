package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.repository.dto.SessionDTORepository;
import ru.t1.rleonov.tm.service.ConnectionService;
import ru.t1.rleonov.tm.service.PropertyService;
import ru.t1.rleonov.tm.service.dto.ProjectDTOService;
import ru.t1.rleonov.tm.service.dto.TaskDTOService;
import ru.t1.rleonov.tm.service.dto.UserDTOService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Date;
import static ru.t1.rleonov.tm.constant.SessionTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.ADMIN;

@Category(UnitCategory.class)
public final class SessionDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROPERTY_SERVICE,
            PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @NotNull
    private static ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String ADMIN_ID = "";

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER2_ID = USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail()).getId();
        ADMIN_ID = USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail()).getId();
    }

    @AfterClass
    @SneakyThrows
    public static void reset() {
        USER_SERVICE.clear();
        CONNECTION_SERVICE.getEntityManagerFactory().close();
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        SESSION1.setUserId(USER1_ID);
        SESSION2.setUserId(USER2_ID);
        SESSION_ADMIN.setUserId(ADMIN_ID);
        SESSION_TEST.setUserId(USER1_ID);
        try {
            transaction.begin();
            repository.add(SESSION1);
            repository.add(SESSION2);
            repository.add(SESSION_ADMIN);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void after() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(SESSION1);
            repository.remove(SESSION2);
            repository.remove(SESSION_ADMIN);
            repository.remove(SESSION_TEST);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull ISessionDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(SESSION_TEST);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable SessionDTO session;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            session = repository.findOneById(SESSION_TEST.getId());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_TEST.getId(), session.getId());
    }

    @Test
    @SneakyThrows
    public void update() {
        SESSION1.setUserId(USER2_ID);
        SESSION1.setRole(Role.ADMIN);
        SESSION1.setCreated(new Date());
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull ISessionDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(SESSION1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable SessionDTO session;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            session = repository.findOneById(SESSION1.getId());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(session);
        Assert.assertEquals(USER2_ID, session.getUserId());
        Assert.assertEquals(Role.ADMIN, session.getRole());
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull ISessionDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(SESSION1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable SessionDTO session;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            session = repository.findOneById(SESSION1.getId());
        } finally {
            entityManager.close();
        }
        Assert.assertNull(session);
    }

}
