package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.ISessionDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import ru.t1.rleonov.tm.exception.entity.SessionNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.service.dto.ProjectDTOService;
import ru.t1.rleonov.tm.service.dto.SessionDTOService;
import ru.t1.rleonov.tm.service.dto.TaskDTOService;
import ru.t1.rleonov.tm.service.dto.UserDTOService;
import static ru.t1.rleonov.tm.constant.SessionTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.ADMIN;

@Category(UnitCategory.class)
public final class SessionDTOServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final ISessionDTOService SESSION_SERVICE = new SessionDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROPERTY_SERVICE,
            PROJECT_SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String ADMIN_ID = "";

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER2_ID = USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail()).getId();
        ADMIN_ID = USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail()).getId();
    }

    @AfterClass
    @SneakyThrows
    public static void reset() {
        USER_SERVICE.clear();
    }

    @Before
    @SneakyThrows
    public void before() {
        SESSION1.setUserId(USER1_ID);
        SESSION2.setUserId(USER2_ID);
        SESSION_ADMIN.setUserId(ADMIN_ID);
        SESSION_TEST.setUserId(USER1_ID);
        SESSION_SERVICE.create(SESSION1);
        SESSION_SERVICE.create(SESSION2);
        SESSION_SERVICE.create(SESSION_ADMIN);
    }

    @After
    @SneakyThrows
    public void after() {
        SESSION_SERVICE.remove(SESSION1);
        SESSION_SERVICE.remove(SESSION2);
        SESSION_SERVICE.remove(SESSION_ADMIN);
        SESSION_SERVICE.remove(SESSION_TEST);
    }

    @Test
    public void create() {
        Assert.assertThrows(SessionNotFoundException.class, () -> SESSION_SERVICE.create(null));
        @Nullable final SessionDTO session = SESSION_SERVICE.create(SESSION_TEST);
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void existsById() {
        @NotNull final String id = SESSION1.getId();
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.existsById(null));
        Assert.assertTrue(SESSION_SERVICE.existsById(id));
    }

    @Test
    public void findOneById() {
        @NotNull final String id = SESSION1.getId();
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.findOneById(null));
        @Nullable final SessionDTO session = SESSION_SERVICE.findOneById(id);
        Assert.assertNotNull(session);
        Assert.assertEquals(id, session.getId());
    }

    @Test
    public void removeById() {
        @NotNull final String id = SESSION1.getId();
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(null));
        @Nullable final SessionDTO session = SESSION_SERVICE.removeById(id);
        Assert.assertNotNull(session);
        Assert.assertEquals(id, session.getId());
        Assert.assertNull(SESSION_SERVICE.findOneById(id));
    }

}
