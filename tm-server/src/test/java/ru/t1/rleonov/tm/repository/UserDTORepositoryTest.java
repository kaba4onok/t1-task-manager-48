package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.repository.dto.UserDTORepository;
import ru.t1.rleonov.tm.service.ConnectionService;
import ru.t1.rleonov.tm.service.PropertyService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserDTORepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static EntityManager getEntityManager() {
        return CONNECTION_SERVICE.getEntityManager();
    }

    @NotNull
    private static IUserDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDTORepository(entityManager);
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void after() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(USERTEST);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull List<UserDTO> users;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(4, users.size());
    }

    @Test
    @SneakyThrows
    public void update() {
        USER1.setLogin("TESTLOGIN");
        USER1.setPassword("TESTPASS");
        USER1.setEmail("TEST@email.ru");
        USER1.setLocked(true);
        USER1.setLastName("TEST_LAST");
        USER1.setFirstName("TEST_FIRST");
        USER1.setMiddleName("TEST_MID");
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(USER1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @Nullable UserDTO user;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            user = repository.findOneById(USER1.getId());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(user);
        Assert.assertEquals("TESTLOGIN", user.getLogin());
        Assert.assertEquals("TESTPASS", user.getPassword());
        Assert.assertEquals("TEST@email.ru", user.getEmail());
        Assert.assertEquals("TEST_LAST", user.getLastName());
        Assert.assertEquals("TEST_FIRST", user.getFirstName());
        Assert.assertEquals("TEST_MID", user.getMiddleName());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @SneakyThrows
    public void set() {
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        after();
        try {
            transaction.begin();
            repository.set(USERS);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull List<UserDTO> users;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(4, users.size());
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(USER1);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull List<UserDTO> users;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(2, users.size());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull List<UserDTO> users;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        try {
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(3, users.size());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        try {
            user = repository.findOneById(USER1.getId());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), USER1.getId());
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        try {
            user = repository.findByLogin(USER1.getLogin());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), USER1.getLogin());
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        try {
            user = repository.findByEmail(USER1.getEmail());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getEmail(), USER1.getEmail());
    }

    @Test
    @SneakyThrows
    public void isLoginExist() {
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        try {
            user = repository.findByLogin(USER1.getLogin());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(user);
    }

    @Test
    @SneakyThrows
    public void isEmailExist() {
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        try {
            user = repository.findByEmail(USER1.getEmail());
        } finally {
            entityManager.close();
        }
        Assert.assertNotNull(user);
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull EntityManager entityManager = getEntityManager();
        @NotNull IUserDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull List<UserDTO> users;
        entityManager = getEntityManager();
        repository = getRepository(entityManager);
        try {
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        Assert.assertEquals(0, users.size());
    }

}
