package ru.t1.rleonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @NotNull String sort);

    @Nullable
    TaskDTO findOneById(@NotNull String userId, @NotNull String id);

    void clear();

    void clear(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
