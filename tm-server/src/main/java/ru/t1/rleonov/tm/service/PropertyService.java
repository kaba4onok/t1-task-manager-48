package ru.t1.rleonov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "1231233";

    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "24343";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "123123";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String DB_DRIVER = "database.driver";

    @NotNull
    private static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DB_LOGIN = "database.username";

    @NotNull
    private static final String DB_LOGIN_DEFAULT = "postgres";

    @NotNull
    private static final String DB_PASSWORD = "database.password";

    @NotNull
    private static final String DB_URL = "database.url";

    @NotNull
    private static final String DB_URL_DEFAULT = "jdbc:postgresql://localhost:5432/tm";

    @NotNull
    private static final String DB_DIALECT = "database.dialect";

    @NotNull
    private static final String DB_SCHEMA = "database.schema";

    @NotNull
    private static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DB_DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DB_DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DB_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DB_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DB_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DB_FORMAT_SQL_DEFAULT = "true";

    @NotNull
    private static final String DB_SECOND_LVL_CASH = "database.second_lvl_cache";

    @NotNull
    private static final String DB_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DB_USE_QUERY_CASH = "database.use_query_cache";

    @NotNull
    private static final String DB_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DB_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DB_HAZEL_CONFIG = "database.config_file_path";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getDbSecondLvlCash() {
        return getStringValue(DB_SECOND_LVL_CASH);
    }

    @NotNull
    @Override
    public String getDbSchema() {
        return getStringValue(DB_SCHEMA);
    }

    @NotNull
    @Override
    public String getDbFactoryClass() {
        return getStringValue(DB_FACTORY_CLASS);
    }

    @NotNull
    @Override
    public String getDbUseQueryCash() {
        return getStringValue(DB_USE_QUERY_CASH);
    }

    @NotNull
    @Override
    public String getDbUseMinPuts() {
        return getStringValue(DB_USE_MIN_PUTS);
    }

    @NotNull
    @Override
    public String getDbRegionPrefix() {
        return getStringValue(DB_REGION_PREFIX);
    }

    @NotNull
    @Override
    public String getDbHazelConfig() {
        return getStringValue(DB_HAZEL_CONFIG);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbDialect() {
        return getStringValue(DB_DIALECT, DB_DIALECT_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbDdlAuto() {
        return getStringValue(DB_DDL_AUTO, DB_DDL_AUTO_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbShowSql() {
        return getStringValue(DB_SHOW_SQL, DB_SHOW_SQL_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbFormatSql() {
        return getStringValue(DB_FORMAT_SQL, DB_FORMAT_SQL_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbDriver() {
        return getStringValue(DB_DRIVER, DB_DRIVER_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbLogin() {
        return getStringValue(DB_LOGIN, DB_LOGIN_DEFAULT);
    }

    @Override
    @NotNull
    public String getDbPassword() {
        return getStringValue(DB_PASSWORD);
    }

    @Override
    @NotNull
    public String getDbUrl() {
        return getStringValue(DB_URL, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}
