package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import javax.persistence.EntityManager;

public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull String id) {
        if (id.isEmpty()) return null;
        return entityManager.find(SessionDTO.class, id);
    }

}
